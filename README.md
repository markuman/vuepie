# vuepie

Just put the index.html somewhere and browse it.

# dynamic data

add e.g. `https://markuman.gitlab.io/vuepie/?data={"Linux":64,"Mac":35,"Windos":1}` to an url.

# git mirror

* https://gitlab.com/markuman/vuepie
* https://github.com/markuman/vuepie

# deployment mirrors

* http://blog.osuv.de/vuepie
* https://markuman.gitlab.io/vuepie/

# made with

* [vuejs](https://vuejs.org/)
* [vue-chartkick](https://github.com/ankane/vue-chartkick)